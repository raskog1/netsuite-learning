/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        10 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/log', 'N/record', 'N/runtime'],
    /**
     * @param{log} log
     */
    function (email, log, record, runtime) {
        function beforeSubmit(context) {
            const customer = context.newRecord;

            if (context.type !== context.UserEventType.CREATE) return;

            const sales_rep = customer.getValue({fieldId: "salesrep"});

            if (!sales_rep) {
                throw "Save failed.  Please make sure that the Sales Rep field is not empty.";
            }
        }

        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} context
         * @param {Record} context.newRecord - New record
         * @param {Record} context.oldRecord - Old record
         * @param {string} context.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        function afterSubmit(context) {
            const customer = context.newRecord;
            const cust_id = customer.id;
            const cust_email = customer.getValue("email");
            const sales_rep = customer.getValue("salesrep");
            const entity = customer.getValue("entityid");
            const cust_name = customer.getValue("glommedname");

            const current_user = runtime.getCurrentUser();

            log.audit("id", cust_id);
            log.audit("email", cust_email);
            log.audit("sales rep", sales_rep);

            if (context.type !== context.UserEventType.CREATE) {
                log.debug("type: ", context.type)
                return;
            }

            const event = record.create({
                type: record.Type.CALENDAR_EVENT,
                isDynamic: true,
            })

            event.setValue({fieldId: "title", value: "Welcome conversation with " + entity});
            event.setValue({fieldId: "company", value: cust_name});

            // Send email notifications
            event.setCurrentSublistValue({
                sublistId: "attendee",
                fieldId: "sendemail",
                value: true
            })

            // Add invite for organizer of event
            event.selectNewLine({
                sublistId: "attendee"
            });
            event.setCurrentSublistValue({
                sublistId: "attendee",
                fieldId: "attendee",
                value: customer.id
            })
            // event.setCurrentSublistText({
            //     sublistId: "attendee",
            //     fieldId: "attendance",
            //     value: "Required" //Should be using an id here
            // })
            event.commitLine({
                sublistId: "attendee"
            })

            // Add invite for Sales Rep.
            event.selectNewLine({
                sublistId: "attendee"
            });
            event.setCurrentSublistValue({
                sublistId: "attendee",
                fieldId: "attendee",
                value: sales_rep
            })
            // event.setCurrentSublistText({
            //     sublistId: "attendee",
            //     fieldId: "attendance",
            //     value: "Required" //Should be using an id here
            // })
            event.commitLine({
                sublistId: "attendee"
            })

            event.save();

            const task = record.create({
                type: record.Type.TASK,
                isDynamic: true,
                defaultValues: {
                    customform: -120
                }
            })

            task.setValue({fieldId: "title", value: "New Customer Follow-up"});
            task.setValue({
                fieldId: "message",
                value: "Please take care of this customer and follow-up with them soon."
            });
            task.setValue({fieldId: "priority", value: "HIGH"});
            task.setValue({fieldId: "company", value: cust_id});

            // Set Sales Rep only if it exists in the record
            // Why does this assign Larry Nelson in the ASSIGNED TO field
            // even when Customer Sales Rep is empty?
            if (sales_rep) task.setValue({fieldId: "assigned", value: sales_rep});

            task.save();

            email.send({
                author: current_user.id,
                recipients: cust_id,
                subject: "Welcome to SuiteDreams",
                body: "Welcome!  We are glad for you to be a customer of SuiteDreams."
            })
        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        }
    });
