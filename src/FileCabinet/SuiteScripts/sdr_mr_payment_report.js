/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        14 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * @NAmdConfig /SuiteScripts/Protelo2/utils/config.json
 */
define(['N/error', 'N/log', 'N/search', 'pi/SearchPI', 'pi/MapReducePI'],
    /**
     * @param{SearchPI} SearchPI
     * @param{MapReducePI} MapReducePI
     */
    function (error, log, search, SearchPI, MapReducePI) {

        /**
         * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
         * @param {Object} inputContext
         * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {Object} inputContext.ObjectRef - Object that references the input data
         * @typedef {Object} ObjectRef
         * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
         * @property {string} ObjectRef.type - Type of the record instance that contains the input data
         * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
         * @since 2015.2
         */
        function getInputData(inputContext) {

            try {
                var customerpaymentSearchObj = search.create({
                    type: "customerpayment",
                    filters:
                        [
                            ["type", "anyof", "CustPymt"],
                            "AND",
                            ["mainline", "is", "T"]
                        ],
                    columns:
                        [
                            search.createColumn({name: "entity", label: "Name"}),
                            search.createColumn({name: "statusref", label: "Status"}),
                            search.createColumn({name: "amountpaid", label: "Amount Paid"})
                        ]
                });
                const pi_search_obj = new SearchPI(customerpaymentSearchObj, {native_columns: true});
                const pi_search_results = pi_search_obj.getResultsByLabel();
                return pi_search_results;
            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'getInputData', details: JSON.stringify(err)});
            }

        }

        /**
         * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
         * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
         * context.
         * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
         *     is provided automatically based on the results of the getInputData stage.
         * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
         *     function on the current key-value pair
         * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
         *     pair
         * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {string} mapContext.key - Key to be processed during the map stage
         * @param {string} mapContext.value - Value to be processed during the map stage
         * @since 2015.2
         */
        function map(context, key, value) {
            try {
                this.emit(key, value);
            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'map', details: JSON.stringify(err)});
            }
        }

        /**
         * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
         * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
         * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
         *     provided automatically based on the results of the map stage.
         * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
         *     reduce function on the current group
         * @param {number} reduceContext.executionNo - Number of times the reduce function has been executed on the current group
         * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {string} reduceContext.key - Key to be processed during the reduce stage
         * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
         *     for processing
         * @since 2015.2
         */
        function reduce(context) {

            try {
                var total = 0;
                for (var i in context.values) {
                    total += parseFloat(context.values[i]);
                }

                log.debug("Totals", "Customer: " + context.key + "\n" +
                    "Total: " + total);
            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'reduce', details: JSON.stringify(err)});
            }

        }


        /**
         * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
         * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
         * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
         * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
         *     script
         * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
         * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
         * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
         * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
         *     script
         * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
         * @param {Object} summaryContext.inputSummary - Statistics about the input stage
         * @param {Object} summaryContext.mapSummary - Statistics about the map stage
         * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
         * @since 2015.2
         */
        function summarize(summary) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'summarize', details: JSON.stringify(err)});
            }

        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };

    });
