/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        14 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/error', 'N/runtime'],
    /**
     * @param{runtime} runtime
     */
    function (currentRecord, error, runtime) {

        /**
         * Function to be executed after page is initialized.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.mode - The mode in which the record is being accessed (create, copy, or edit)
         *
         * @since 2015.2
         */
        function pageInit(context) {

            console.log("Client Script");

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('pageInit : ' + JSON.stringify(err));
            }

        }

        /**
         * Function to be executed when field is changed.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         * @param {string} context.fieldId - Field name
         * @param {number} context.lineNum - Line number. Will be undefined if not a sublist or matrix field
         * @param {number} context.columnNum - Line number. Will be undefined if not a matrix field
         *
         * @since 2015.2
         */
        function fieldChanged(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.fieldChanged : ' + JSON.stringify(err));
            }

        }

        /**
         * Function to be executed when field is slaved.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         * @param {string} context.fieldId - Field name
         *
         * @since 2015.2
         */
        function postSourcing(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.postSourcing : ' + JSON.stringify(err));
            }

        }

        /**
         * Function to be executed after sublist is inserted, removed, or edited.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         *
         * @since 2015.2
         */
        function sublistChanged(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.sublistChanged : ' + JSON.stringify(err));
            }

        }

        /**
         * Function to be executed after line is selected.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         *
         * @since 2015.2
         */
        function lineInit(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.lineInit : ' + JSON.stringify(err));
            }

        }

        /**
         * Validation function to be executed when field is changed.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         * @param {string} context.fieldId - Field name
         * @param {number} context.lineNum - Line number. Will be undefined if not a sublist or matrix field
         * @param {number} context.columnNum - Line number. Will be undefined if not a matrix field
         *
         * @returns {boolean} Return true if field is valid
         *
         * @since 2015.2
         */
        function validateField(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.validateField : ' + JSON.stringify(err));
            }

            return true;

        }

        /**
         * Validation function to be executed when sublist line is committed.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         *
         * @returns {boolean} Return true if sublist line is valid
         *
         * @since 2015.2
         */
        function validateLine(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.validateLine : ' + JSON.stringify(err));
            }

            return true;

        }

        /**
         * Validation function to be executed when sublist line is inserted.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         *
         * @returns {boolean} Return true if sublist line is valid
         *
         * @since 2015.2
         */
        function validateInsert(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.validateInsert : ' + JSON.stringify(err));
            }

            return true;

        }

        /**
         * Validation function to be executed when record is deleted.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @param {string} context.sublistId - Sublist name
         *
         * @returns {boolean} Return true if sublist line is valid
         *
         * @since 2015.2
         */
        function validateDelete(context) {

            try {

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.validateDelete : ' + JSON.stringify(err));
            }

            return true;

        }

        /**
         * Validation function to be executed when record is saved.
         *
         * @param {Object} context
         * @param {Record} context.currentRecord - Current form record
         * @returns {boolean} Return true if record is valid
         *
         * @since 2015.2
         */
        function saveRecord(context) {

            try {
                // var record_type = context.currentRecord.type;
                var record_type = runtime.getCurrentScript().getParameter({
                    name: "custscript_sdr_save_record_type"
                });
                var checked = runtime.getCurrentScript().getParameter({
                    name: "custscript_sdr_save_confirmation"
                });

                if (checked) {
                    confirm("Click OK if you are sure you would like to submit your changes for this " + record_type + " record.  Click Cancel to continue editing");
                }

            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message
                };
                console.log('Client.saveRecord : ' + JSON.stringify(err));
            }

            return true;

        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            pageInit: pageInit,
            fieldChanged: fieldChanged,
            postSourcing: postSourcing,
            sublistChanged: sublistChanged,
            lineInit: lineInit,
            validateField: validateField,
            validateLine: validateLine,
            validateInsert: validateInsert,
            validateDelete: validateDelete,
            saveRecord: saveRecord
        };

    });
