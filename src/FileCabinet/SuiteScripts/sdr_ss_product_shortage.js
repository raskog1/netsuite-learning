/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        14 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.1
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/log', 'N/record', 'N/search'],

    function (error, log, record, search) {

        /**
         * Defines the Scheduled script trigger point.
         * @param {Object} context
         * @param {string} context.type - Script execution context. Use values from the scriptContext.InvocationType enum.
         * @since 2015.2
         */
        function execute(context) {

            try {
                var customrecord_sdr_prod_prefSearchObj = search.create({
                    type: "customrecord_sdr_prod_pref",
                    filters:
                        [
                            ["custrecord_sdr_prod_pref_qty", "greaterthan", "2"],
                            "AND",
                            ["custrecord_sdr_prod_pref_customer.subsidiary", "anyof", "1"]
                        ],
                    columns:
                        [
                            search.createColumn({name: "custrecord_sdr_prod_pref_customer", label: "Customer"}),
                            search.createColumn({
                                name: "email",
                                join: "CUSTRECORD_SDR_PROD_PREF_CUSTOMER",
                                label: "Email"
                            }),
                            search.createColumn({
                                name: "subsidiary",
                                join: "CUSTRECORD_SDR_PROD_PREF_CUSTOMER",
                                label: "Primary Subsidiary"
                            }),
                            search.createColumn({name: "custrecord_sdr_prod_pref_item", label: "Item"}),
                            search.createColumn({name: "custrecord_sdr_prod_pref_qty", label: "Preferred Quantity"}),
                            search.createColumn({
                                name: "quantityavailable",
                                join: "CUSTRECORD_SDR_PROD_PREF_ITEM",
                                label: "Available"
                            })
                        ]
                });
                var searchResults = customrecord_sdr_prod_prefSearchObj.run().getRange({
                    start: 0,
                    end: 9
                });

                for (var i = 0; i < searchResults.length; i++) {
                    var customer = searchResults[i].getValue("custrecord_sdr_prod_pref_customer");
                    var email = searchResults[i].getText({
                        name: "email",
                        join: "CUSTRECORD_SDR_PROD_PREF_CUSTOMER"
                    });
                    var subsidiary = searchResults[i].getText({
                        name: "subsidiary",
                        join: "CUSTRECORD_SDR_PROD_PREF_CUSTOMER"
                    });
                    var qty = parseInt(searchResults[i].getValue({
                        name: "quantityavailable",
                        join: "CUSTRECORD_SDR_PROD_PREF_ITEM"
                    }));
                    var item = searchResults[i].getText("custrecord_sdr_prod_pref_item");
                    var pref_qty = parseInt(searchResults[i].getValue("custrecord_sdr_prod_pref_qty"));

                    log.debug("Info", "Customer: " + customer + "\n" +
                        "Pref: " + pref_qty + "\n" +
                        "Item: " + item + "\n" +
                        "Qty Avail.: " + qty
                    )

                    if (qty < pref_qty) {
                        var support_case = record.create({
                            type: record.Type.SUPPORT_CASE,
                            isDynamic: true
                        })

                        support_case.setValue({fieldId: "title", value: "Item low for customer"});
                        support_case.setValue({fieldId: "company", value: customer});
                        support_case.setValue({fieldId: "email", value: "test@test.com"});
                        support_case.setValue({fieldId: "incomingmessage", value: "This company " +
                                "prefers to purchase " + pref_qty + " " + item + " each time they " +
                                "create a sales order, but only " + qty + " are left in stock."});

                        support_case.save();
                    }
                }


            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'execute', details: JSON.stringify(err)});
            }

        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            execute: execute
        };

    });
