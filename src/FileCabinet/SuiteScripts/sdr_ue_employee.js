/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        10 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/log', 'N/record'],
    /**
     * @param{log} log
     */
    function (log, record) {
        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} context
         * @param {Record} context.newRecord - New record
         * @param {Record} context.oldRecord - Old record
         * @param {string} context.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        function afterSubmit(context)
        {
            // if (context.type !== context.UserEventType.CREATE) {
            //     log.debug("type: ", context.type)
            //     return;
            // }


        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            afterSubmit: afterSubmit
        }
    });
