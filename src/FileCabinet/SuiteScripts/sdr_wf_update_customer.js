/**
 * Module Description
 *
 * Version        Date                Author           Remarks
 * 1.00        17 Jan 2022            ryans
 *
 *
 * Copyright (c) 2022 Protelo, Inc. All Rights Reserved.
 *
 * Protelo reserves all rights in the Software as delivered. The Software or any portion thereof may not be reproduced in any form whatsoever, except as provided by license, without the written
 * consent of Protelo. A license under Protelo's rights in the Software may be available directly from Protelo.
 *
 * THIS NOTICE MAY NOT BE REMOVED FROM THE SOURCE FOR ANY REASON.
 *
 * NEITHER PROTELO NOR ANY PERSON OR ORGANIZATON ACTING ON BEHALF OF PROTELO MAKE ANY REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER PROTELO NOR ANY PERSON OR ORGANIZATION ACTON ON BEHALF OF PROTELO SHALL BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * @NApiVersion 2.1
 * @NScriptType workflowactionscript
 * @NModuleScope SameAccount
 * @NAmdConfig /SuiteScripts/Protelo2/utils/config.json
 */
define(['N/error', 'N/log', 'N/record', 'N/runtime'],
    /**
     * @param{record} record
     * @param{runtime} runtime
     */
    function (error, log,  record, runtime) {

        /**
         * Defines the WorkflowAction script trigger point.
         * @param {Object} context
         * @param {Record} context.newRecord - New record
         * @param {Record} context.oldRecord - Old record
         * @param {string} context.workflowId - Internal ID of workflow which triggered this action
         * @param {string} context.type - Event type
         * @param {Form} context.form - Current form that the script uses to interact with the record
         * @since 2016.1
         */
        function onAction(context) {

            try {
                var order_date = context.getCurrentScript().getParameter({
                    name: "custscript_sdr_order_date"
                });

                var order_rec = context.newRecord;

                var line_count = order_rec.getLineCount({
                    sublistId: "item"
                })

                var notes = "Last Order Date: " + order_date + "\n" +
                    "Unique Items Ordered: " + line_count;

                var cust_id = order_rec.getValue({fieldId: "entity"});


                log.debug("id", cust_id);
                var customer = record.submitField({
                    type: record.Type.CUSTOMER,
                    id: cust_id,
                    values: {
                        comments: notes
                    }
                })

                // customer.setValue({fieldId: "comments", value: notes})
                customer.save();
            } catch (e) {
                var nle = error.create(e);
                err = {
                    stacktrace: e.stack,
                    reasoncode: nle.name,
                    message: nle.message,
                    event: nle.eventType,
                    id: nle.id,
                    internalid: nle.recordid
                };
                log.error({title: 'onAction', details: JSON.stringify(err)});
            }

        }

        /**
         * @param {Any} Value or object
         * @returns {Boolean} Is parameter value empty
         */
        function isEmpty(stValue) {
            if ((stValue == '') || (stValue == null) || (stValue == undefined) || (stValue.length == 0)) {
                return true;
            }
            return false;
        }

        return {
            onAction: onAction
        };

    });
